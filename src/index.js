import { GraphQLServer } from 'graphql-yoga'
import uuidv4 from 'uuid/v4';

// Scalar types - String, Boolean, Int, Float, ID, [type]

const users = [
  {
    id: '1',
    username: 'YUN',
    email: 'yuncrystal@gmail.com',
    age: 28
  },
  {
    id: '2',
    username: 'Krystal',
    email: 'Krystal@gmail.com',
    age: 27
  },
  {
    id: '3',
    username: 'Jackson',
    email: 'Jackson@gmail.com',
    age: 24
  }
]

const posts = [
  {
    id: '1',
    title: 'Java',
    body: 'Java is a good programming language',
    published: true,
    author: '1'
  },
  {
    id: '2',
    title: 'JS',
    body: 'JS is a good programming language',
    published: true,
    author: '2'
  },
  {
    id: '3',
    title: 'PHP',
    body: 'PHP is a good programming language',
    published: true,
    author: '3'
  }
]

const comments = [
  {
    id: '1',
    text: 'I am comment 1',
    author: '1',
    post:'1'
  },
  {
    id: '21',
    text: 'I am comment 2',
    author: '2',
    post:'2'
  },
  {
    id: '3',
    text: 'I am comment 3',
    author: '3',
    post:'3'
  },
  {
    id: '4',
    text: 'I am comment 4',
    author: '2',
    post:'1'
  }
]

/**
 *  type Query: 定义查询api 接口
 *  type Mutation: 定义增删改api 接口
 *  其他：自定义类型
 */


const typeDefs = `

  type Query {
    users(query:String):[User!]!
    posts(query:String):[Post!]!
    comments:[Comment!]!
    product:Product
    greeting(name:String):String
    add(numbers:[Float]!):Float!
    grades:[Int!]!
  }

  type Mutation{
    createUser(username:String!,email:String!, age:Int):User!
    createPost(title:String!,body:String!, published:Boolean!,author: ID!):Post!
    createComment(text:String!,author: ID!, post: ID!):Comment!
  }

  type User{
    id:ID!
    username:String!
    email:String!
    age:Int
    posts:[Post]
    comments:[Comment]
  }

  type Post{
    id:ID!
    title:String!
    body:String!
    published:Boolean!
    author:User!
    comments:[Comment]
  }

  type Comment{
    id:ID!
    text:String!
    author:User!
    post:Post!
  }

  type Product{
    id:ID!
    brand:String!
    category:String
    description:String
    img_main:String
    name_zh:String!
    name_en:String!
    purchase_price:Float!
    freight:Float
    exchange_rate:Float
    cost_price:Float
    sales_price_au:Float
    sales_price_cny:Float
    storage:Int
  }

`
// Resolvers
const resolvers = {
  Query: {
    users: (_, args) => {
      if (!args.query) {
        return users
      }

      return users.filter((user) => {
        return user.username.toLowerCase().includes(args.query.toLowerCase())
      })
    },

    posts: (_, { query }) => {
      if (!query) {
        return posts
      }

      return posts.filter((post) => {
        let isTitleMatch = post.title.toLowerCase().includes(query.toLowerCase());
        let isBodyMatch = post.body.toLowerCase().includes(query.toLowerCase())
        return isTitleMatch || isBodyMatch
      })
    },

    comments: () => {
      return comments
    },

    product: (parent, args, ctx, info) => {
      return {
        id: '22sss',
        brand: 'swiss',
        name_zh: '中文名',
        name_en: '英文名',
        purchase_price: 2.5
      }
    },
    greeting: (_, { name }) => `Hello ${name || 'World'}`,
    add: (_, { numbers }) => {
      if (numbers.length === 0) {
        return 0
      } else {
        return numbers.reduce((sum, currentValue) => {
          return sum + currentValue
        })
      }
    },
    grades: () => {
      return [1, 2, 3]
    }
  },

/** Resolver for 增删改 */
  Mutation: {
    createUser(parent,args,ctx,info){
      const emailToken = users.some((user)=>{
        return user.email === args.email
      })

      if(emailToken) {
        throw new Error("Email taken")
      }

      const user = {
        id:uuidv4(),
        username:args.username,
        email:args.email,
        age:args.age,
      }

      users.push(user)
      return user;

    },

    createPost:(_,args)=>{
      const isUserExist = users.some((user)=>{
        return user.id  === args.author
      }) 

      if(!isUserExist) {
        throw new Error("User not find")
      }

      const post = {
        id:uuidv4(),
        title:args.title,
        body:args.body,
        published:args.published,
        author:args.author,
      }

      posts.push(post)
      return post
    },

    createComment:(_,args)=>{
      const isUserExist = users.some((user)=>{
        return user.id  === args.author
      }) ;

      const isPostExist = posts.some((post)=>{
        return post.id  === args.post && post.published
      }) 

      if(!isUserExist || !isPostExist){
        throw new Error("Unable to find user or post")
      }

      const comment = {
        id:uuidv4(),
        text:args.text,
        author:args.author,
        post:args.post,
      }

      comments.push(comment);
      return comment;
    }
  },

  // Relation Type
  /** 当自定义的类型中，存在的一个属性是另外一个自定义类型，需要在这里重新定义两个类型之间的关系
   *  比如下面的例子
   *  Post 类型中有一个属性author, 是User类型的，需要在这里定义Post 的 author 和 User之间的关系
   *  每一个post 都会调用下面的author() function
   */

  Post: {
    author(parent, args, ctx, info) {
      return users.find((user) => {
        return user.id === parent.author
      })
    },

    comments(parent){
      return comments.filter((comment) => {
        return parent.id === comment.post
      })
    }
  },

  User: {
    posts(parent) {
      return posts.filter((post) => {
        return parent.id === post.author
      })
    },

    comments(parent) {
      return comments.filter((comment) => {
        return parent.id === comment.author
      })
    }
  },

  Comment: {
    author(parent, args, ctx, info) {
      return users.find((user) => {
        return user.id === parent.author
      })
    },

    post(parent){
      return posts.find((post) => {
        return post.id === parent.post
      })
    }
  }


}

const server = new GraphQLServer({ typeDefs, resolvers })
server.start(() => console.log('Server is running on localhost:4000'))